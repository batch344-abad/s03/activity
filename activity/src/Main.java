import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Input an integer whose factorial will be computed");

        Scanner in = new Scanner(System.in);

        try {
            int num = in.nextInt();
            int answer = 1;
            int counter = 1;

            if (num < 0) {
                System.out.println("Factorial undefined for negative numbers.");
            } else if (num == 0) {
                System.out.println("Factorial of 0 = 1");
            } else {
                while (counter <= num) {
                    answer *= counter;
                    counter++;
                }
                System.out.println("(while-loop)The factorial of "+ num + " is " + answer);


                answer = 1;
                for (counter = 1; counter <= num; counter++) {
                    answer *= counter;
                }
                System.out.println("(for-loop)The factorial of "+ num + " is " + answer);
            }
        } catch (Exception e) {
            System.out.println("Invalid input");
            e.printStackTrace();
        }
    }
}
